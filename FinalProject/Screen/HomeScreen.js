import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, ActivityIndicator, FlatList } from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import AboutScreen from './AboutScreen';
import Icon  from 'react-native-vector-icons/FontAwesome';

const Tab = createBottomTabNavigator();

export function MyTabs() {
    return (
      <Tab.Navigator>
        <Tab.Screen name="Home" component={HomeScreen}options={{tabBarIcon : () => <Icon size={20} name="home"/>}}/>
        <Tab.Screen name="About" component={AboutScreen} options={{tabBarIcon : () => <Icon size={20} name="user"/>}}/>
      </Tab.Navigator>
    );
  }

function HomeScreen() {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch('https://reactnative.dev/movies.json')
      .then((response) => response.json())
      .then((json) => setData(json.movies))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  return (

    <View style ={styles.container}>
      <View style={styles.header}>
        <Text style={styles.title}>MOVIE LIST</Text>
      </View>
      <View style={styles.body}>
        {isLoading ? <ActivityIndicator/> : (
        <FlatList 
          data={data}
          keyExtractor={({ id }, index) => id}
          renderItem={({ item }) => (
          <Text style ={styles.movieText}>{item.id}. {item.title} - {item.releaseYear}</Text>
          )}
        />
      )}
      </View>
    </View>

    
  );
};




const styles = StyleSheet.create({
    container :{
      flex : 1,
    },
    header : {
      flex : 1,
      backgroundColor : '#5390EB',
      justifyContent : 'center'
    },
    body : {
      flex : 3,
      padding : 15
    },
    movieText :{
      fontSize : 26,
      fontWeight : "bold",
    },
    title : {
      fontSize : 32,
      fontWeight : 'bold'
    }
})





    // <View style={{ flex: 1, padding: 24 }}>

    // </View>