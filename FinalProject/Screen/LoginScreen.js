import 'react-native-gesture-handler';
import * as React from 'react'
import { View, Text, StyleSheet, TouchableOpacity, TextInput} from 'react-native'
import {MyTabs} from './HomeScreen'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';


const Stack = createStackNavigator();

function MyStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="LogIn" component={LogIn} />
      <Stack.Screen name="Home" component={MyTabs} />
    </Stack.Navigator>
  );
}

export default function LoginScreen() {
  return (
      <NavigationContainer>
    <MyStack />
  </NavigationContainer>
  )
}

function LogIn({ navigation }) {
    return (
      
      <View style={styles.container}>
         
         <View style={styles.header}>
           <Text style={styles.judul}>SELAMAT DATANG!!</Text>
         </View>
         <View style={styles.body}>
              <Text style ={styles.subjudul}>LogIn</Text>
              <View style={styles.inputbody}>
                    <Text style={{fontSize : 15}}>Username</Text>
                    <TextInput style={styles.input}/>
                    <Text style={{fontSize : 15}}>Password</Text>
                    <TextInput style={styles.input}/>
              </View>
              <TouchableOpacity style={styles.button}
                onPress = {() => navigation.navigate('Home')}>
                <Text>LogIn</Text>
                </TouchableOpacity>

         </View>

         
      </View>
    );
  }
  

  


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#EFEFEF',

      },
    
    header :{
      flex : 2,
      borderBottomColor : 'skyblue',
      borderBottomWidth : 5,
      justifyContent : 'flex-end'
    },

    body :{
      flex : 8,
      paddingBottom : 50,
      alignItems : 'center',
      justifyContent : 'center'
    },

    judul :{
      fontSize :25,
      fontFamily : 'notoserif',
      fontWeight : 'bold'
    },
    subjudul :{
      fontSize : 20,
      fontFamily : 'monospace',
      fontWeight : 'bold'
    }, 
    input : {
      width : 247,
      height : 45,
      borderWidth: 1,
      borderColor: '#000',
      borderRadius : 10      
    },
    inputbody : {
      padding : 50
    },
    button : {
    width : 137,
    height : 32,
    borderRadius : 10,
    alignItems: "center",
    backgroundColor: "#00D422",
    padding: 10
    }

})
