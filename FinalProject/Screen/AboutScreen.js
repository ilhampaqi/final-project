import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/FontAwesome'

export default function AboutScreen() {
    return (
        <View style = {styles.container}>
         
            <View style ={styles.header}>
                <Icon name="user-circle-o" size={150}/>
                <Text style={styles.name}>ILHAM PAQI ANDRIYANSAH</Text>
                <Text>React Native Developer</Text>
            </View>
            <View style ={styles.portofolio}>
                <Text style={styles.judul}>PORTOFOLIO</Text>
                <View style={styles.Pstyle}>
                    <Icon name = "gitlab" size={30}/>
                    <Text style={{fontSize : 30}}>@ilhampaqi</Text>
                </View>
            </View>
            <View style ={styles.sosmed}>
                <Text style={styles.judul}>SOSIAL MEDIA</Text>
                <View style={styles.Sstyle}>
                    <View style ={{flexDirection : 'row'}}>
                    <Icon name = "facebook" size={30}/>
                    <Text style={{fontSize : 20}}>Paqi gas'ah</Text>
                    </View>
                    <View style ={{flexDirection : 'row'}}>
                    <Icon name = "instagram" size={30}/>
                    <Text style={{fontSize : 20}}>@faqi_ilham</Text>
                    </View>
                </View>
                <View style ={{flexDirection : 'row', alignItems : 'center',justifyContent:'center'}}>
                    <Icon name = "twitter" size={30}/>
                    <Text style={{fontSize : 20}}>@unexpected_paqi</Text>
                    </View>
            </View>
            
        </View>
    )
}

const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor : '#ffff'
    },
    header : {
        flex : 5,
        justifyContent : 'center',
        alignItems : 'center',
        borderBottomColor : '#0984e3',
        borderBottomWidth : 10
    },
    portofolio :{
        flex : 2,

        borderBottomColor : '#17c0eb',
        borderBottomWidth : 5
    },
    Pstyle :{
        padding : 15,
        alignItems : 'center',
        flexDirection : 'row',
        justifyContent : 'center'
    },
    
    Sstyle :{
        padding : 20,
        alignItems : 'center',
        flexDirection : 'row',
        justifyContent : 'space-around'
    },
    sosmed : {
        flex : 3,

    },
    name :{
        fontSize : 30,
        fontFamily : 'monospace'
    }, 
    judul :{
        fontFamily : 'monospace',
        fontSize : 20,
        
    }
})
